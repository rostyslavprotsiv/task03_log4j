package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.CellPhoneAction;
import com.rostyslavprotsiv.model.entity.CellPhone;
import com.rostyslavprotsiv.model.exceptions.CellPhoneLogicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private static final Controller INSTANCE = new Controller();
    private static final CellPhoneAction ACTION = CellPhoneAction.getInstance();
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    private Controller() { } // private constructor
    public static Controller getInstance() {
        return INSTANCE;
    }

    public double getPriceOfAll() {
        return ACTION.countPriceOfRandomList();
    }

    public String getCreatedPhone(final double price, final String name,
                                  final boolean hasTouchScreen) {
        try {
            return new CellPhone(price, name, hasTouchScreen).toString();
        } catch (CellPhoneLogicalException e) {
            LOGGER.trace(e.getMessage());
            LOGGER.debug(e.getMessage());
            LOGGER.info(e.getMessage());
            LOGGER.warn(e.getMessage());
            LOGGER.error(e.getMessage());
            LOGGER.fatal(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
