package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

/**
 * This class is created for
 * showing menu for users.
 * @author Rostyslav Protsiv
 * @version 1.1.0 14 April 2019
 * @since 1.0.0
 */
public class Menu {
    public static final Controller CONTROLLER = Controller.getInstance();
    public static final Scanner SCANNER = new Scanner(System.in,"UTF-8");
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public final void show() {
        //simulation of communication between user and system
        int out;
        System.out.println("What do you want ?");
        System.out.println("1.Out price of all randomly created phones.");
        System.out.println("2.Input parameters.");
        out = SCANNER.nextInt();
        if (out == 1) {
            System.out.println(CONTROLLER.getPriceOfAll());
        } else if (out == 2) {
            outObject();
        } else {
            LOGGER.trace("Entered incorrect value : " + out);
            LOGGER.debug("Entered incorrect value : " + out);
            LOGGER.info("Entered incorrect value : " + out);
            LOGGER.warn("Entered incorrect value : " + out);
            LOGGER.error("Entered incorrect value : " + out);
            LOGGER.fatal("Entered incorrect value : " + out);
        }
        System.out.println("The end");
    }

    private void outObject() {
        double price;
        String name;
        boolean hasTouchScreen;
        System.out.println("Please, input price : ");
        price = SCANNER.nextDouble();
        System.out.println("Please, input name : ");
        name = SCANNER.next();
        System.out.println("Please, input hasTouchScreen : ");
        hasTouchScreen = SCANNER.nextBoolean();
        System.out.println(CONTROLLER.getCreatedPhone(price,
                name, hasTouchScreen));
    }

}
