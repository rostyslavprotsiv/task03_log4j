package com.rostyslavprotsiv.model.exceptions;

public class CellPhoneLogicalException extends Exception {
    public CellPhoneLogicalException() {
    }

    public CellPhoneLogicalException(final String s) {
        super(s);
    }

    public CellPhoneLogicalException(final String s,
                                     final Throwable throwable) {
        super(s, throwable);
    }

    public CellPhoneLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public CellPhoneLogicalException(final String s, final Throwable throwable,
                                     final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}
