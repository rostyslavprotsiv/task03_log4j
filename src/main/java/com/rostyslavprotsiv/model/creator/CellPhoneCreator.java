package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.CellPhone;
import com.rostyslavprotsiv.model.exceptions.CellPhoneLogicalException;
import static com.rostyslavprotsiv.model.entity.CellPhone.*;
import java.util.ArrayList;
import java.util.Random;

public class CellPhoneCreator {
    private static final Random RANDOM = new Random();
    public final ArrayList<CellPhone> createList(final int size) {
        final ArrayList<CellPhone> createdList = new ArrayList<>();
        final int minAppropriateChar = 33;
        final int maxAppropriateChar = 126;
        final int maxSizeOfName = 15;
        boolean hasTouchScreen;
        StringBuilder name;
        int sizeOfName;
        double price;
        for (int i = 0; i < size; i++) {
            hasTouchScreen = RANDOM.nextBoolean();
            name = new StringBuilder();
            sizeOfName = RANDOM.nextInt(maxSizeOfName);
            price = RANDOM.nextDouble() * MAX_PRICE;
            for (int j = 0; j < sizeOfName; j++) {
                name.append(getRandomCharFromRange(minAppropriateChar,
                        maxAppropriateChar));
            }
            try {
                createdList.add(new CellPhone(price, name.toString(), hasTouchScreen));
            } catch (CellPhoneLogicalException e) {
                e.printStackTrace();
            }
        }
        return createdList;
    }

    private char getRandomCharFromRange(final int from, final int to) {
        return (char) (RANDOM.nextInt(to - from) + from);
    }
}
