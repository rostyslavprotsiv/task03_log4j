package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.CellPhoneLogicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Objects;

public class CellPhone {
    private double price;
    private String name;
    private boolean hasTouchScreen;
    public static final double MAX_PRICE = 999999;
    private static final Logger LOGGER = LogManager.getLogger(CellPhone.class);

    public CellPhone() {
    }

    public CellPhone(final double price, final String name,
                     final boolean hasTouchScreen)
            throws CellPhoneLogicalException {
        if (checkPrice(price)) {
            this.price = price;
            this.name = name;
            this.hasTouchScreen = hasTouchScreen;
        } else {
            LOGGER.trace("This is a trace message");
            LOGGER.debug("This is a debug message");
            LOGGER.info("This is an info message");
            LOGGER.warn("This is a warn message");
            LOGGER.error("This is an error message");
            LOGGER.fatal("This is a fatal message");
            throw new CellPhoneLogicalException();
        }
    }

    public final double getPrice() {
        return price;
    }

    public final void setPrice(final double price)
            throws CellPhoneLogicalException {
        if (checkPrice(price)) {
            this.price = price;
        } else {
            LOGGER.warn("This is a warn message");
            LOGGER.error("This is an error message");
            LOGGER.fatal("This is a fatal message");
            throw new CellPhoneLogicalException();
        }
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final boolean isHasTouchScreen() {
        return hasTouchScreen;
    }

    public final void setHasTouchScreen(final boolean hasTouchScreen) {
        this.hasTouchScreen = hasTouchScreen;
    }

    private boolean checkPrice(final double price) {
        return price > 0 && price < MAX_PRICE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CellPhone cellPhone = (CellPhone) o;
        return Double.compare(cellPhone.price, price) == 0
                && hasTouchScreen == cellPhone.hasTouchScreen
                && Objects.equals(name, cellPhone.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name, hasTouchScreen);
    }

    @Override
    public String toString() {
        return "CellPhone{"
                + "price=" + price
                + ", name='" + name
                + '\'' + ", hasTouchScreen="
                + hasTouchScreen + '}';
    }
}
