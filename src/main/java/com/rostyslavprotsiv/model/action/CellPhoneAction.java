package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.CellPhone;

import java.util.ArrayList;

public final class CellPhoneAction {
    private static final CellPhoneAction INSTANCE = new CellPhoneAction();

    private CellPhoneAction() { }

    public static CellPhoneAction getInstance() {
        return INSTANCE;
    }

    public double countPrice(final ArrayList<CellPhone> phones) {
        double price = 0;
        for (CellPhone phone: phones) {
            price += phone.getPrice();
        }
        return price;
    }

    public double countPriceOfRandomList() {
        ArrayList<CellPhone> createdPrice = new ArrayList<>();
        return countPrice(createdPrice);
    }
}
